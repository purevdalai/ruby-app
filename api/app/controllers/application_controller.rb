class ApplicationController < ActionController::Base # API changed to Base because Basic authenticate in Base
  http_basic_authenticate_with name: ENV.fetch('BASIC_AUTH_USER'), password: ENV.fetch('BASIC_AUTH_PASSWORD')
  rescue_from ActiveRecord::RecordNotFound, with: :render_api_404

  def render_api_404
    render plain: '', status: 404
  end
end

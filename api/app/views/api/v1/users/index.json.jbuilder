json.users do |json|
  json.array! @users, partial: 'api/v1/users/user', as: :user
end

# DApps API (back end)

### Enivorment
- Rails version 5.2.3
- Ruby 2.5.3p105 (2018-10-18 revision 65156) [x86_64-linux]
- MySQL 5.7

### How to develop

#### Create environment variables
- Create `.env` file inside your project's root folder  
  ※ you can rename `dot.env.example` to `.env`
- Update environment variables
```
DATABASE_HOST=<Database hostname or IP>
DATABASE_PORT=<Database port>
DATABASE_SCHEMA=<Database schema name>
DATABASE_USER=<Database user name>
DATABASE_PASSWORD=<Database user password>

BASIC_AUTH_USER=<API basic auth user name>
BASIC_AUTH_PASSWORD=<API basic auth user password>
```

#### Execute application
- Execute application `rails s`
- Check application `http://localhost:4000/api/v1/users/` in browser

#### Check codes
- Check ruby codes `rubocop`